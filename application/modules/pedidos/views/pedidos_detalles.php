<?php 
    switch($pedido->tipo_pedidos_id){
         case 1://Mesa
             echo '<h1>Mesa: '.$this->db->get_where('mesas',array('id'=>$pedido->mesas_id))->row()->mesa_nombre.'</h1>';
         break;
         case 2://deliverys
            echo '<h1>Delivery: '.$this->db->get_where('deliverys',array('id'=>$pedido->deliverys_id))->row()->nombre_delivery.'</h1>';
         break;
         case 3://Barra
            echo '<h1>Barra: '.$this->db->get_where('barras',array('id'=>$pedido->barras_id))->row()->nombre_barra.'</h1>';
         break;
    }
?>
<?= $output ?>
<script>
    $("#field-total, #field-precio_venta,#field-precio_costo").attr('readonly',true);
   $("#field-productos_id").on('change',function(){
       var id = parseInt($(this).val());
       if(!isNaN(id)){
            $.post('<?= base_url('movimientos/productos/productos/json_list') ?>',{search_field:'id',search_text:id},function(data){
                data = JSON.parse(data);
                data = data[0];
                $("#field-cantidad").val(1);
                $("#field-precio_venta").val(data.precio_venta);
                $("#field-precio_costo").val(data.precio_costo);
                totalizar();
            });
        }
   });
   $("#field-cantidad").on('change',function(){
    totalizar();
   });
   
   function totalizar(){
       var cantidad = parseFloat($("#field-cantidad").val());
       var precio = parseFloat($("#field-precio_venta").val());
       if(!isNaN(cantidad) && !isNaN(precio)){
           $("#field-total").val(cantidad*precio);
       }
   }
</script>