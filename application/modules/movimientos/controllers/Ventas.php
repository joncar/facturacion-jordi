<?php

require_once APPPATH.'/controllers/Panel.php';    

class Ventas extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }                
    }
    
    public function ventas($x = '', $y = '',$z = '') {
        $this->load->library('enletras');        
        if(empty($x) || $x=='success' || $x=='ajax_list' || $x=='ajax_list_info'){
            $this->as['ventas'] = 'view_ventas';
        }
        $crud = parent::crud_function($x, $y);

        $crud->set_theme('bootstrap2');
        $crud->set_primary_key('id');
        $crud->unset_read();                
        if($crud->getParameters()=='list'){
            $crud->field_type('status','dropdown',array('1'=>'<span class="label label-danger">Pendiente</span>','2'=>'<span class="label label-success"></i> Cobrada'));
        }else{
            $crud->field_type('status','dropdown',array('1'=>'Pendiente','2'=>'Cobrada'));
        }
        $accion = '';
        if($x=='cobrar'){                        
            $crud->fields('status','fecha_cobro');
            if($crud->getParameters()=='list'){
                redirect('movimientos/ventas/ventas');
            }
        }
        $crud->columns('fecha','nrofactura_int','j4983a0ab.razon_social','j4983a0ab.nombre_comercial','subtotal','iva','irpf','total','status','observacion');        
        $crud->callback_column('nrofactura_int',function($val,$row){
            return '<a href="'.base_url('reportes/rep/verReportes/1/pdf/venta/'.$row->id).'" target="_blank">'.$row->nro_factura.'</a>';
        })->display_as('nrofactura_int','#Factura');
        $crud->callback_before_insert(function($post){
            $post['fecha_facturacion'] = date("Y-m-d",strtotime(str_replace('/','-',$post['fecha_facturacion'])));
            $post['fecha_vencimiento'] = date("Y-m-d",strtotime(str_replace('/','-',$post['fecha_vencimiento'])));
            $post['caja'] = $this->user->caja;
            $post['cajadiaria'] = $this->user->cajadiaria;
            $post['sucursal'] = $this->user->sucursal;
            $post['fecha'] = date("Y-m-d");
            $post['usuario'] = $this->user->id;
            $post['status'] = 1;
            return $post;
        });
        $crud->callback_after_insert(function($post,$primary){
            if(!empty($_POST['venta'])){                
                foreach($_POST['venta']['producto'] as $n=>$v){                    
                    if(!empty($_POST['venta']['producto'][$n])){
                        $this->db->insert('ventadetalle',array(
                            'venta'=>$primary,
                            'producto'=>$_POST['venta']['producto'][$n],
                            'precio'=>$_POST['venta']['precio'][$n],
                            'cantidad'=>$_POST['venta']['cantidad'][$n],
                            'total'=>$_POST['venta']['total'][$n],
                        ));
                    }
                }
            }

            $caja = get_instance()->db->get_where('cajadiaria',array('caja'=>$_SESSION['caja'],'abierto'=>1));
            $correlativo = $caja->row()->correlativo+1;
            get_instance()->db->update('cajadiaria',array('correlativo'=>$correlativo),array('caja'=>$_SESSION['caja'],'abierto'=>1));
        });
        $crud->callback_before_update(function($post){
            $post['fecha_facturacion'] = date("Y-m-d",strtotime(str_replace('/','-',$post['fecha_facturacion'])));
            $post['fecha_vencimiento'] = date("Y-m-d",strtotime(str_replace('/','-',$post['fecha_vencimiento'])));
            return $post;
        });
        $crud->callback_after_update(function($post,$primary){            
            if(!empty($_POST['venta'])){                
                get_instance()->db->delete('ventadetalle',array('venta'=>$primary));
                foreach($_POST['venta']['producto'] as $n=>$v){                    
                    if(!empty($_POST['venta']['producto'][$n])){                  
                        $this->db->insert('ventadetalle',array(
                            'venta'=>$primary,
                            'producto'=>$_POST['venta']['producto'][$n],
                            'precio'=>$_POST['venta']['precio'][$n],
                            'cantidad'=>$_POST['venta']['cantidad'][$n],
                            'total'=>$_POST['venta']['total'][$n],
                        ));
                    }
                }
            }
        });
        $crud->set_relation('transaccion', 'tipotransaccion', 'denominacion')
                ->set_relation('cliente', 'clientes', '{razon_social}|{nombre_comercial}')
                ->set_relation('sucursal', 'sucursales', 'denominacion');
        
        $crud->display_as('j4983a0ab.razon_social','Razon Social')
                 ->display_as('j4983a0ab.nombre_comercial','Nombre Comercial');
        $crud->callback_column('j4983a0ab.razon_social',function($val,$row){return explode('|',$row->s4983a0ab)[0];});
        $crud->callback_column('j4983a0ab.nombre_comercial',function($val,$row){return explode('|',$row->s4983a0ab)[1];});
        
        $crud->where('sucursal', $_SESSION['sucursal']);
        if (!empty($_SESSION['caja'])){
            $crud->where('caja', $_SESSION['caja']);
        }
        $crud->where('status <',3);
        $crud->order_by('fecha','DESC');
        $output = $crud->render($accion);
        $output->crud = 'ventas';
        if ($x == 'add') {
            $output->output = $this->load->view('ventas', array(), TRUE);
        }else if ($x == 'edit' && !empty($y) && is_numeric($y)) {
            $venta = $this->db->get_where('ventas', array('id' => $y));
            $detalles = $this->db->get_where('ventadetalle', array('venta' => $venta->row()->id));
            $output->output = $this->load->view('ventas', array('venta' => $venta->row(), 'detalles' => $detalles), TRUE);
        }
        $this->loadView($output);
    }
    
    function next_nro_factura(){
        echo $this->querys->get_nro_factura();
    }
    /* Cruds */
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
