<?php

require_once APPPATH.'/controllers/Panel.php';    

class Presupuestos extends Panel {

    function __construct() {
        parent::__construct();
        if($this->router->fetch_method()!='selsucursal' && empty($this->user->sucursal)){
            header("Location:".base_url('panel/selsucursal'));
            die();
        }
        if($this->router->fetch_method()!='selcaja' && empty($this->user->caja)){
            header("Location:".base_url('panel/selcaja'));
            die();
        }
        if($this->router->fetch_method()!='selcajadiaria' && empty($this->user->cajadiaria)){
            header("Location:".base_url('panel/selcajadiaria'));
            die();
        }                
    }
    
    public function presupuestos($x = '', $y = '') { 
        if($x=='facturar' && is_numeric($y)){
            //Guardamos el nro de factura y editamos
            $factura = $this->db->get_where('ventas',array('id'=>$y));
            if($factura->num_rows()>0){
                $this->db->update('ventas',array('nro_factura'=>$this->querys->get_nro_factura(),'status'=>1),array('id'=>$y));

                $caja = $this->db->get_where('cajadiaria',array('caja'=>$_SESSION['caja'],'abierto'=>1));
                $correlativo = $caja->row()->correlativo+1;
                $this->db->update('cajadiaria',array('correlativo'=>$correlativo),array('caja'=>$_SESSION['caja'],'abierto'=>1));

                $this->db->update('hostings',array('status'=>2),array('factura'=>$y));
                redirect('movimientos/ventas/ventas/edit/'.$y);
            }
            die();
        }
        $this->as['presupuestos'] = 'ventas';
        $this->load->library('enletras');        
        $crud = parent::crud_function($x, $y);
        $crud->set_theme('bootstrap2');
        $crud->unset_read();                        
        $accion = '';        
        $crud->columns('id','nro_presupuesto','cliente','subtotal','iva','irpf','total');
        $crud->callback_column('nro_presupuesto',function($val,$row){
            $val = empty($val)?'###':$val;
            return '<a href="'.base_url('reportes/rep/verReportes/2/pdf/venta/'.$row->id).'" target="_blank">'.$val.'</a>';
        });
        $crud->callback_before_insert(function($post){
            $post['fecha_facturacion'] = date("Y-m-d",strtotime(str_replace('/','-',$post['fecha_facturacion'])));
            $post['fecha_vencimiento'] = date("Y-m-d",strtotime(str_replace('/','-',$post['fecha_vencimiento'])));
            $post['caja'] = $this->user->caja;
            $post['cajadiaria'] = $this->user->cajadiaria;
            $post['sucursal'] = $this->user->sucursal;
            $post['fecha'] = date("Y-m-d");
            $post['usuario'] = $this->user->id;
            $post['status'] = 3;
            return $post;
        });
        $crud->callback_after_insert(function($post,$primary){
            if(!empty($_POST['venta'])){                
                foreach($_POST['venta']['producto'] as $n=>$v){                    
                    if(!empty($_POST['venta']['producto'][$n])){
                        $this->db->insert('ventadetalle',array(
                            'venta'=>$primary,
                            'producto'=>$_POST['venta']['producto'][$n],
                            'precio'=>$_POST['venta']['precio'][$n],
                            'cantidad'=>$_POST['venta']['cantidad'][$n],
                            'total'=>$_POST['venta']['total'][$n],
                        ));
                    }
                }
            }        
        });
        $crud->callback_before_update(function($post){
            $post['fecha_facturacion'] = date("Y-m-d",strtotime(str_replace('/','-',$post['fecha_facturacion'])));
            $post['fecha_vencimiento'] = date("Y-m-d",strtotime(str_replace('/','-',$post['fecha_vencimiento'])));
            return $post;
        });
        $crud->callback_after_update(function($post,$primary){            
            if(!empty($_POST['venta'])){                
                get_instance()->db->delete('ventadetalle',array('venta'=>$primary));
                foreach($_POST['venta']['producto'] as $n=>$v){  
                    if(!empty($_POST['venta']['producto'][$n])){                  
                        $this->db->insert('ventadetalle',array(
                            'venta'=>$primary,
                            'producto'=>$_POST['venta']['producto'][$n],
                            'precio'=>$_POST['venta']['precio'][$n],
                            'cantidad'=>$_POST['venta']['cantidad'][$n],
                            'total'=>$_POST['venta']['total'][$n],
                        ));
                    }
                }
            }
        });
        $crud->set_relation('transaccion', 'tipotransaccion', 'denominacion')
                ->set_relation('cliente', 'clientes', '{razon_social}')
                ->set_relation('sucursal', 'sucursales', 'denominacion');
        $crud->where('ventas.sucursal', $_SESSION['sucursal']);
        if (!empty($_SESSION['caja'])){
            $crud->where('ventas.caja', $_SESSION['caja']);
        }
        $crud->where('status',3);
        $crud->order_by('id','DESC');
        $output = $crud->render($accion);
        $output->crud = 'ventas';
        if ($x == 'add') {
            $output->output = $this->load->view('presupuestos', array(), TRUE);
        }else if ($x == 'edit' && !empty($y) && is_numeric($y)) {
            $venta = $this->db->get_where('ventas', array('id' => $y));
            $detalles = $this->db->get_where('ventadetalle', array('venta' => $venta->row()->id));
            $output->output = $this->load->view('presupuestos', array('venta' => $venta->row(), 'detalles' => $detalles), TRUE);
        }
        $this->loadView($output);
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
