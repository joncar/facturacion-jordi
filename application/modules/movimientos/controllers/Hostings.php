<?php

require_once APPPATH.'/controllers/Panel.php';    

class Hostings extends Main {

    function __construct() {
        parent::__construct();
    }
    
    public function hostings($x = '',$y = '',$z = '') {
    	$fecha = date("Y-m-d");
    	//Sumamos 5 dias
    	//$fecha = date("Y-m-d",strtotime($fecha.' +5 days'));
        $fecha = date("Y-m-d",strtotime($fecha));
    	$proximas = $this->db->get_where('hostings',array('DATE(fecha_vencimiento) <= '=>''.$fecha.''));
        foreach($proximas->result() as $p){

        	$data = array(        		
        		'transaccion'=>1,
                'cliente'=>$p->cliente,
        		'fecha_facturacion'=>date("Y-m-d"),
        		'fecha_vencimiento'=>date("Y-m-d"),
        		'fecha'=>date("Y-m-d"),
        		'status'=>3,
        		'cajadiaria'=>1,
        		'forma_pago'=>2,
        		'usuario'=>2778,
        		'subtotal'=>$p->importe,
        		'irpf'=>$p->importe*0.15,
        		'iva'=>$p->importe*0.21,
        		'total'=>$p->importe-($p->importe*0.15)+($p->importe*0.21),
        		'sucursal'=>1,
        		'caja'=>1
        	);

        	$id = $this->db->insert('ventas',$data);
        	$id = $this->db->insert_id();

        	$data = array(
        		'venta'=>$id,
        		'producto'=>'Renovació domini i hospedatge de '.$p->dominio,
        		'cantidad'=>1,
        		'precio'=>$p->importe,
        		'total'=>$p->importe
        	);

        	$this->db->insert('ventadetalle',$data);
        	$fecha = strtotime($p->fecha_vencimiento);
        	$fecha = (date("Y",$fecha)+1).'-'.date("m-d",$fecha);
        	$this->db->update('hostings',array('status'=>1,'factura'=>$id,'fecha_vencimiento'=>$fecha),array('id'=>$p->id));

        	$msj = '<h1>Presupuesto cargado por renovación de hosting</h1>';
        	$msj.= '<h3>Datos de facturación</h3>';
        	$msj.= '<ul>';
        	$msj.= '<li><b>Cliente: </b>'.$p->cliente.'</li>';
        	$msj.= '<li><b>Dominio: </b>'.$p->dominio.'</li>';
        	$msj.= '<li><b>Importe: </b>'.$p->importe.' €</li>';
        	$msj.= '<li><b>Fecha de vencimiento: </b>'.date("d/m/Y",strtotime($p->fecha_vencimiento)).'</li>';
        	$msj.= '</ul>';
        	$msj.= '<p><a href="'.base_url('movimientos/presupuestos/presupuestos/edit/'.$id).'">Enlace del presupuesto</a></p>';
        	correo('eliana@jordimagana.com','Renovación pendiente de '.$p->dominio,$msj);
        }

        //correo('joncar.c@gmail.com','TEST','EJECUTADO');
    }
}

/* End of file panel.php */
/* Location: ./application/controllers/panel.php */
