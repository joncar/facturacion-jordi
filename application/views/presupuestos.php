<div class="alert <?= !empty($_SESSION['msj'])?'alert-success':'' ?>" style="<?= !empty($_SESSION['msj'])?'':'display:none' ?>"><?= !empty($_SESSION['msj'])?$_SESSION['msj']:'' ?></div>
<?php $_SESSION['msj'] = !empty($_SESSION['msj'])?'':'' ?>
<form class="form-horizontal" id='formulario' onsubmit="return val_send(this)" role="form">
    <div class="well">
    <div class="row">
        <div class="col-xs-3">
            <div class="form-group ui-widget">
              <label for="proveedor" class="col-sm-4 control-label">Cliente: </label>
              <div class="col-sm-8" id="cliente_div">
                  <?php $sel = empty($venta)?0:$venta->cliente; ?>
                  <?= form_dropdown_from_query('cliente','clientes','id','razon_social',$sel,'id="cliente"') ?>
              </div>
            </div>
        </div> 
        <div class="col-xs-3">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Transacción: </label>
              <div class="col-sm-8">
                  <?php $sel = empty($venta)?0:$venta->transaccion; ?>
                  <?= form_dropdown_from_query('transaccion',$this->db->get('tipotransaccion'),'id','denominacion',$sel); ?>
              </div>
            </div>
        </div> 
        <div class="col-xs-2">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Fecha: </label>
              <div class="col-sm-8">
                  <input type="text" name="fecha" value="<?= empty($venta)?date("d/m/Y H:i:s"):date("d/m/Y H:i:s",strtotime($venta->fecha)) ?>" id="fecha" class="datetime-input form-control">
              </div>
            </div>
        </div>
        <div class="col-xs-4">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Nro. Pres: </label>
              <div class="col-sm-8">
                  <input type="text" class="form-control" id="nro_presupuesto" name="nro_presupuesto" value="<?= empty($venta)?'':$venta->nro_presupuesto; ?>">
              </div>
            </div>
        </div> 
    </div>
    <div class="row">
        <div class="col-xs-3">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Fecha de fact.: </label>
              <div class="col-sm-8">
                  <input type="text" name="fecha_facturacion" value="<?= empty($venta)?date("d/m/Y"):date("d/m/Y H:i:s",strtotime($venta->fecha_facturacion)) ?>" id="fecha_fact" class="date-input form-control">
              </div>
            </div>
        </div> 
        <div class="col-xs-3">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Fecha de venc.: </label>
              <div class="col-sm-8">
                  <input type="text" name="fecha_vencimiento" value="<?= empty($venta)?date("d/m/Y"):date("d/m/Y H:i:s",strtotime($venta->fecha_vencimiento)) ?>" id="fecha_venc" class="date-input form-control">
              </div>
            </div>
        </div> 
        <div class="col-xs-3">
            <div class="form-group">
              <label for="proveedor" class="col-sm-4 control-label">Forma Pago: </label>
              <div class="col-sm-8">
                  <?php $sel = empty($venta)?0:$venta->forma_pago; ?>
                  <?= form_dropdown_from_query('forma_pago',$this->db->get('formapago'),'id','denominacion',$sel) ?> 
              </div>
            </div>
        </div> 
        <div class="col-xs-3">
            <div class="form-group">
              <label for="proveedor" class="col-sm-1 control-label">Observ.: </label>
              <div class="col-sm-11">
                  <input type="text" value="<?= empty($venta)?'':$venta->observacion; ?>" class="form-control" name="observacion" id="observacion" placeholder="Observacion">
              </div>
            </div>
        </div>         
    </div>  
    </div>
    <div class="row">        
        <div class="col-xs-12">
            <table class="table table-striped" style="font-size:12px;" cellspacing="2" id="detall">
                <thead>
                    <tr>                        
                        <th>Nombre artículo</th>                        
                        <th>Cant.</th>                        
                        <th>Precio</th>                                                
                        <th>Total</th>                            
                        <th>Acciones</th>
                    </tr>
                </thead>
                <tbody>
                    <?php $filas = 0; ?>
                    <?php if(!empty($venta)): ?>
                        <?php $detalles = $this->db->get_where('ventadetalle',array('venta'=>$venta->id)); ?>
                        <?php foreach($detalles->result() as $n=>$d): ?>
                            <tr>                                
                                <td><textarea name="venta[producto][]" data-name="producto" type="text" class="form-control producto" placeholder="Nombre"><?= $d->producto ?></textarea></td>
                                <td><input name="venta[cantidad][]" data-name="cantidad" type="text" class="form-control cantidad" placeholder="Cantidad"  value="<?= $d->cantidad ?>"></td>                                                                
                                <td><input name='venta[precio][]' data-name="precio" type="text" class="form-control precio" placeholder="Precio" value="<?= $d->precio ?>"></td>                                                                                
                                <td><input name='venta[total][]' data-name="total" type="text" class="form-control total" placeholder="Total" value="<?= $d->total ?>"></td>                                                
                                <td><p align="center">
                                        <a href="#" class="addrow"><i class="fa fa-plus"></i></a> 
                                        <a href="#" class="remrow"><i class="fa fa-minus"></i></a>
                                    </p></td>
                            </tr>
                        <?php endforeach ?>
                    <?php endif ?>
                    <tr>                        
                        <td><textarea name="venta[producto][]" data-name="producto" type="text" class="form-control producto" placeholder="Nombre"></textarea></td>
                        <td><input name="venta[cantidad][]" data-name="cantidad" type="text" class="form-control cantidad" placeholder="Cantidad"  value=""></td>
                        <td><input name='venta[precio][]' data-name="precio" type="text" class="form-control precio" placeholder="Precio" value=""></td>                        
                        <td><input name='venta[total][]' data-name="total" type="text" class="form-control total" placeholder="Total" value=""></td>
                        <td><p align="center">
                                <a href="#" class="addrow"><i class="fa fa-plus"></i></a> 
                                <a href="#" class="remrow"><i class="fa fa-minus"></i></a>
                            </p></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
    <div class="row">
      <div class="col-xs-12 col-sm-4 col-sm-offset-8">
        <label style="width:100%">
            Subtotal
            <input type="text" name="subtotal" class="form-control subtotal" value="<?= !empty($venta)?$venta->subtotal:0 ?>">
        </label>
      </div>
      <div class="col-xs-12 col-sm-4 col-sm-offset-8">
        <label style="width:100%">
            IVA
            <input type="text" name="iva" class="form-control iva" value="<?= !empty($venta)?$venta->iva:0 ?>">
        </label>
      </div>
      <div class="col-xs-12 col-sm-4 col-sm-offset-8">
        <label style="width:100%">
            IRPF
            <input type="text" name="irpf" class="form-control irpf" value="<?= !empty($venta)?$venta->irpf:0 ?>">
        </label>
      </div>
      <div class="col-xs-12 col-sm-4 col-sm-offset-8">
        <label style="width:100%">
            TOTAL
            <input type="text" name="total" class="form-control totalventa" value="<?= !empty($venta)?$venta->total:0 ?>">
        </label>
      </div>
    </div>
    <input type="hidden" name="nro_factura" value="0">
    <div class="row" style="margin-top:40px">        
        <a href="<?= base_url('maestras/clientes/add/json') ?>" target="_new" class="btn btn-default">Agregar Nuevo Cliente</a>        
        <button type="submit" id="guardar" class="btn btn-success">Guardar Venta</button>
    </div>
</form>
<?php $this->load->view('predesign/datepicker',array('scripts'=>'a')) ?>
<?= $this->load->view('predesign/chosen.php') ?>
<script>    
    function addrow(obj){
        var row = $(obj).parents('tr');
            row.after('<tr>'+row.html()+'</tr>');
            $(".total").attr('readonly',true);
    }
    function removerow(obj){
        var o = $(obj).parents('tbody');
        $(obj).parent('td').parent('tr').remove();                
        $(document).trigger('total');
    }
    
    $("body").on('click','.addrow',function(e){
        e.preventDefault();        
        addrow($(this).parent('p'));
    });

    $("body").on('click','.remrow',function(e){
        e.preventDefault();
        removerow($(this).parent('p'));
    }); 
    
   $("body").on('change','.cantidad,.precio',function(){
       $(document).trigger('total');
    });
      
    var subtotal = 0,iva,irpf,total;  
    $(document).on('total',function(){      
        subtotal = 0;
        $(".precio").each(function(){
          var row = $(this).parents('tr');
          var cantidad = parseFloat(row.find('.cantidad').val());
          var precio = parseFloat(row.find('.precio').val());
          if(!isNaN(cantidad) && !isNaN(precio)){
            var totali = cantidad*precio;
            row.find('.total').val(totali);
            subtotal+= totali;
            $(".subtotal").val(subtotal);
            iva = subtotal*0.21;
            $(".iva").val(iva.toFixed(2));
            irpf = subtotal*0.07;
            $(".irpf").val(irpf.toFixed(2));
            total = subtotal+iva-irpf;
            $(".totalventa").val(total.toFixed(2));
          }
        });
    });
    var ajax = undefined;
    var validation_url = '<?= empty($venta)?base_url('movimientos/presupuestos/presupuestos/insert_validation'):base_url('movimientos/presupuestos/presupuestos/update_validation/'.$venta->id) ?>';
    var url = '<?= empty($venta)?base_url('movimientos/presupuestos/presupuestos/insert'):base_url('movimientos/presupuestos/presupuestos/update/'.$venta->id) ?>';
    function val_send(form){
       //if(ajax==undefined){
        $(".mask").show();
        var data = document.getElementById('formulario');
        $("#guardar").attr('disabled','disabled');
        $(document).trigger('total');
        datos = new FormData(data);
        ajax = $.ajax({
            url:validation_url,
            method:'post',
            data:datos,
            processData:false,
            cache: false,
            contentType: false,
            success:function(data){
              console.log(data);
                data = data.replace('<textarea>','',data);
                data = data.replace('</textarea>','',data);
                data = JSON.parse(data);
                if(data.success){
                  $.ajax({
                      url:url,
                      method:'post',
                      data:datos,
                      processData:false,
                      cache: false,
                      contentType: false,
                      success:function(data){
                          data = data.replace('<textarea>','',data);
                          data = data.replace('</textarea>','',data);
                          data = JSON.parse(data);
                          if(data.success){
                              document.location.href = "<?= base_url('movimientos/presupuestos/presupuestos/success') ?>";
                          } else{
                              $(".alert").removeClass('alert-success').addClass('alert-danger').html(data.error_message).show();
                              $(".mask").hide();
                              $("#guardar").removeAttr('disabled');
                              ajax = undefined;                
                          }               
                      },
                      error:function(data){
                          $(".mask").hide();
                          ajax = undefined;
                          emergente('Ocurrio un error intentando guardar el registro, por favor intente de nuevo');
                      }
                  });
                } else{
                    $(".alert").removeClass('alert-success').addClass('alert-danger').html(data.error_message).show();
                    $(".mask").hide();
                    $("#guardar").removeAttr('disabled');
                    ajax = undefined;                
                }               
            },
            error:function(data){
                $(".mask").hide();
                ajax = undefined;
                emergente('Ocurrio un error intentando guardar el registro, por favor intente de nuevo');
            }
            });
       /*}
      else emergente('Se esta enviando el registro, por favor espere!!...');*/
      return false;
    }    
</script>
<div class="mask" style="background:rgba(0,0,0,.8); width:100%; height:100%; position:fixed; top:0px; left:0px; display:none;"></div>