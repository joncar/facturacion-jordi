<div class="row" style="margin:40px;">
    <div class="space-6"></div>

    <div class="col-sm-12 infobox-container">        

        <div class="infobox infobox-blue">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-money"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $this->db->query(' SELECT FORMAT(SUM(total),0,"de_DE") as total from ventas where status = 1 and cajadiaria = '.$this->user->cajadiaria)->row()->total ?></span>
                <div class="infobox-content">Facturas por cobrar</div>
            </div>
        </div>

        <div class="infobox infobox-blue">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-money"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $this->db->query(' SELECT FORMAT(SUM(total),0,"de_DE") as total from ventas where status = 2 and cajadiaria = '.$this->user->cajadiaria)->row()->total ?></span>
                <div class="infobox-content">Facturas Cobradas</div>
            </div>
        </div>

        
        <div class="infobox infobox-blue">
            <div class="infobox-icon">
                <i class="ace-icon fa fa-money"></i>
            </div>

            <div class="infobox-data">
                <span class="infobox-data-number"><?= $this->db->query(' SELECT FORMAT(SUM(total),0,"de_DE") as total from ventas where (status = 1 OR status = 2) and cajadiaria = '.$this->user->cajadiaria)->row()->total ?></span>
                <div class="infobox-content">Total Facturas</div> 
            </div>
        </div>
    </div> 
    <div class="vspace-12-sm"></div>

    <div class="col-sm-5">
        
    </div><!-- /.col -->
</div>


